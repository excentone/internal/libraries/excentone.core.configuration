﻿using System.Diagnostics.CodeAnalysis;

namespace ExcentOne.Core.Configuration.Settings
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ActiveDirectory
    {
        public string LDAP { get; set; }

        public string Username { get; set; }

        public string EncryptedPassword { get; set; }

        public Connection SyncDatabase { get; set; }
    }
}