﻿namespace ExcentOne.Core.Configuration.Settings
{
    public class Application
    {
        public string Id { get; set; }
        public string HashKey { get; set; }
    }
}