﻿using System.ComponentModel;

namespace ExcentOne.Core.Configuration.Settings
{
    public class K2
    {
        [DefaultValue("localhost")] public string Server { get; set; }

        [DefaultValue(5252)] public int Port { get; set; }

        [DefaultValue(false)] public bool IsIntegrated { get; set; }

        [DefaultValue(true)] public bool IsPrimaryLogin { get; set; }

        [DefaultValue(true)] public bool IsAuthenticate { get; set; }

        [DefaultValue(false)] public bool IsEncryptedPassword { get; set; }

        [DefaultValue("K2")] public string SecurityLabelName { get; set; }

        public string UserId { get; set; }

        public string EncryptedPassword { get; set; }

        public string ServiceAccount { get; set; }

        [DefaultValue("ASP")] public string Platform { get; set; }

        public string Processes { get; set; }

        [DefaultValue("Pending Requestor Action")]
        public string OriginatorActivityNames { get; set; }
    }
}