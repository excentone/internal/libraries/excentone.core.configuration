﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using ExcentOne.Core.Configuration.Settings;
using ExcentOne.Core.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ExcentOne.Core.Configuration
{
    public class ExcentOneConfiguration
    {
        public ExcentOneSettings ExcentOneSettings { get; set; }
    }

    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public class ExcentOneSettings
    {
        public static string SectionKey = "ExcentOneSettings";

        internal static ExcentOneSettings CurrentSettings;

        private ExcentOneSettings()
        {
            Application = new Application();
            K2 = new K2();
            Flow = new Flow();
            Connections = new Connections();
            Smtps = new Smtps();
        }

        public static ExcentOneSettings Current
        {
            get
            {
                if (CurrentSettings == null || CurrentSettings.Application.HashKey.Equals(string.Empty))
                    CurrentSettings = GetConfigurationSettingsFromJson();
                return CurrentSettings;
            }
        }

        public Application Application { get; set; }

        public K2 K2 { get; set; }

        public Flow Flow { get; set; }

        public Connections Connections { get; set; }

        public ActiveDirectory ActiveDirectory { get; set; }

        public Smtps Smtps { get; set; }

        public static ExcentOneSettings GetConfigurationSettingsFromJsonFile(string path)
        {
            return GetExcentOneSettings(path);
        }

        private static ExcentOneSettings GetConfigurationSettingsFromJson()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            configurationBuilder.AddJsonFile("appsettings.json", false, true);
            configurationBuilder.AddJsonFile($"appsettings.{environmentName}.json", true,
                true);
            var configuration = configurationBuilder.Build();

            var configFile = configuration[SectionKey];
            return GetExcentOneSettings(configFile);
        }

        private static ExcentOneSettings GetExcentOneSettings(string configFile)
        {
            var configContent = File.ReadAllText(configFile);

            var eoSettings = JsonConvert.DeserializeObject<ExcentOneConfiguration>(configContent);
            if (eoSettings == null) throw new NullReferenceException("Excent One Settings failed to retrieve");

            if (eoSettings.ExcentOneSettings.ActiveDirectory != null &&
                !eoSettings.ExcentOneSettings.ActiveDirectory.EncryptedPassword.Equals(string.Empty))
                eoSettings.ExcentOneSettings.ActiveDirectory.EncryptedPassword =
                    eoSettings.ExcentOneSettings.ActiveDirectory.EncryptedPassword.Decrypt(
                        eoSettings.ExcentOneSettings.Application.HashKey, false);

            eoSettings.ExcentOneSettings.Flow.EncryptedPassword =
                eoSettings.ExcentOneSettings.Flow.EncryptedPassword.Decrypt(
                    eoSettings.ExcentOneSettings.Application.HashKey, false);
            eoSettings.ExcentOneSettings.K2.EncryptedPassword =
                eoSettings.ExcentOneSettings.K2.EncryptedPassword.Decrypt(
                    eoSettings.ExcentOneSettings.Application.HashKey, false);

            foreach (var connection in eoSettings.ExcentOneSettings.Connections)
                connection.EncryptedPassword =
                    connection.EncryptedPassword.Decrypt(eoSettings.ExcentOneSettings.Application.HashKey, false);

            eoSettings.ExcentOneSettings.Connections.ReInitialize();
            eoSettings.ExcentOneSettings.Flow.ReInitialize(eoSettings.ExcentOneSettings.Connections);
            eoSettings.ExcentOneSettings.ActiveDirectory?.SyncDatabase.ReInitialize(eoSettings.ExcentOneSettings
                .Connections);

            foreach (var smtp in eoSettings.ExcentOneSettings.Smtps)
                smtp.EncryptedPassword =
                    smtp.EncryptedPassword.Decrypt(eoSettings.ExcentOneSettings.Application.HashKey, false);

            eoSettings.ExcentOneSettings.Smtps.ReInitialize();

            return eoSettings.ExcentOneSettings;
        }
    }
}